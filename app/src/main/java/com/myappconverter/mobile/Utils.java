package com.myappconverter.mobile;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class Utils {

    static public View addSwipeView(RelativeLayout view, Context context){

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View swipeView = layoutInflater.inflate(R.layout.swipe_layout, null);
        RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(
        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        relativeLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        swipeView.setLayoutParams(relativeLayoutParams);
        view.addView(swipeView);
        return swipeView;
    }

    static public View adaptGridViewElement(int value ,View view, Context context){
        view.getLayoutParams().height=dp2pixel(value,context);
        for(int index=0; index<((ViewGroup)view).getChildCount(); ++index) {
            View nextChild = ((ViewGroup)view).getChildAt(index);
            if(nextChild instanceof RelativeLayout){
                ViewGroup.LayoutParams p = nextChild.getLayoutParams();
                RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(p.width,p.height);
                nextChild.setLayoutParams(param);
                break;
            }
        }
        return view;
    }

    static public int dp2pixel(int value, Context context){
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (value * scale + 0.5f);
        return pixels;
    }

}
