package com.myappconverter.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Intent;
import android.widget.TextView;
import android.graphics.Typeface;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import com.myappconverter.mobile.Utils;
import com.myappconverter.mobile.OnSwipeTouchListener;
import com.myappconverter.mobile.CallBack;
import android.content.res.AssetManager;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.myappconverter.mobile.adapter.BaseAdapter_OBy_Bm_mWr;


public class Activity_a6Q_iy_QD2 extends Fragment implements CallBack{

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.getView() == null) {

            View baseView = inflater.inflate(R.layout.activity_a6q_iy_qd2, container, false);

            return baseView;
        }
        return this.getView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    
		ListView listView_OBy_Bm_mWr = (ListView) view.findViewById(R.id.OBy_Bm_mWr);
		listView_OBy_Bm_mWr.setAdapter(new BaseAdapter_OBy_Bm_mWr(this));
	}
	@Override
	public void perform(int position){
		switch (position){
			case 0:
					
			changeFragment(new Activity_imN_EB_O5z());
			break;

		}
	}
	protected void changeFragment(android.support.v4.app.Fragment targetFragment) {
		String backStateName = targetFragment.getClass().getName();
		FragmentManager manager = getChildFragmentManager();
		manager.popBackStackImmediate(backStateName, 0);
		manager.beginTransaction().replace(R.id.main_Activity_jy5_m0_4LL, targetFragment).addToBackStack(backStateName).commit();
	}

	protected AssetManager getAssets() {
		return getActivity().getAssets();
	}

}