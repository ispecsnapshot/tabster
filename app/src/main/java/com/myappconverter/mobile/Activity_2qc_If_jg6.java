package com.myappconverter.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Intent;
import android.widget.TextView;
import android.graphics.Typeface;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import com.myappconverter.mobile.Utils;
import com.myappconverter.mobile.OnSwipeTouchListener;
import com.myappconverter.mobile.CallBack;
import android.content.res.AssetManager;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;


public class Activity_2qc_If_jg6 extends Fragment {

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.getView() == null) {

            View baseView = inflater.inflate(R.layout.activity_2qc_if_jg6, container, false);

            return baseView;
        }
        return this.getView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    
	}

	protected void changeFragment(android.support.v4.app.Fragment targetFragment) {
		String backStateName = targetFragment.getClass().getName();
		FragmentManager manager = getChildFragmentManager();
		manager.popBackStackImmediate(backStateName, 0);
		manager.beginTransaction().replace(R.id.main_Activity_d1A_em_EPA, targetFragment).addToBackStack(backStateName).commit();
	}

	protected AssetManager getAssets() {
		return getActivity().getAssets();
	}

}