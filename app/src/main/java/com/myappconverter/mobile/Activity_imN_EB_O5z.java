package com.myappconverter.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Intent;
import android.widget.TextView;
import android.graphics.Typeface;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import com.myappconverter.mobile.Utils;
import com.myappconverter.mobile.OnSwipeTouchListener;
import com.myappconverter.mobile.CallBack;
import android.content.res.AssetManager;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.myappconverter.mobile.adapter.BaseAdapter_R6Q_Ki_k5Y;


public class Activity_imN_EB_O5z extends Fragment {

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.getView() == null) {

            View baseView = inflater.inflate(R.layout.activity_imn_eb_o5z, container, false);

            return baseView;
        }
        return this.getView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    
		ListView listView_R6Q_Ki_k5Y = (ListView) view.findViewById(R.id.R6Q_Ki_k5Y);
		listView_R6Q_Ki_k5Y.setAdapter(new BaseAdapter_R6Q_Ki_k5Y());		TextView titleBackBarButton = (TextView) view.findViewById(R.id.nav_item_back_title);
        if(titleBackBarButton!=null) {
            titleBackBarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getChildFragmentManager().beginTransaction()
                            .remove(Activity_imN_EB_O5z.this)
                            .commit();
                }
            });
        }


	}

	protected void changeFragment(android.support.v4.app.Fragment targetFragment) {
		String backStateName = targetFragment.getClass().getName();
		FragmentManager manager = getChildFragmentManager();
		manager.popBackStackImmediate(backStateName, 0);
		manager.beginTransaction().replace(R.id.main_Activity_kNY_zL_ef8, targetFragment).addToBackStack(backStateName).commit();
	}

	protected AssetManager getAssets() {
		return getActivity().getAssets();
	}

}