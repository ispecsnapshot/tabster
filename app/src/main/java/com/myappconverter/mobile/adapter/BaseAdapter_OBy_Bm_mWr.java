package com.myappconverter.mobile.adapter;

import com.myappconverter.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.myappconverter.mobile.CallBack;

public class BaseAdapter_OBy_Bm_mWr extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private CallBack mCallBack;
		public BaseAdapter_OBy_Bm_mWr(CallBack callBack){mCallBack = callBack;}

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(layoutInflater==null)
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (position){

				case 0:
                convertView = layoutInflater.inflate(R.layout.activity_cell_nza_fz_xpl, null);
                break;

				case 1:
                convertView = layoutInflater.inflate(R.layout.activity_a6q_iy_qd2_cell_1, null);
                break;

				case 2:
                convertView = layoutInflater.inflate(R.layout.activity_a6q_iy_qd2_cell_2, null);
                break;

				case 3:
                convertView = layoutInflater.inflate(R.layout.activity_a6q_iy_qd2_cell_3, null);
                break;

			}

		convertView.setTag(position);
        
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			mCallBack.perform((Integer) v.getTag());
		}});
        return convertView;
    }
}
