package com.myappconverter.mobile;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.HashMap;

public class Activity_8YX_ce_x5E extends AppCompatActivity{

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    public TabLayout tabLayout;

    private int previousIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_8yx_ce_x5e);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOffscreenPageLimit(99);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (tabLayout.getTabAt(position).getIcon() != null)
                        tabLayout.getTabAt(position).getIcon().setTint(Color.parseColor("#007ccd"));
                }
                if (previousIndex != position) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        if (tabLayout.getTabAt(previousIndex).getIcon() != null)
                            tabLayout.getTabAt(previousIndex).getIcon().setTint(Color.parseColor("#929292"));
                    }
                    previousIndex = position;
                }
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mPager);

        View view = findViewById(android.R.id.content);


		int checkExistence;


    	checkExistence = this.getResources().getIdentifier("tab2", "drawable", this.getPackageName());
        if ( checkExistence != 0 ) {
            tabLayout.getTabAt(1).setIcon(this.getResources().getIdentifier("tab2", "drawable", this.getPackageName()));
        }
        else {
            // tabLayout.getTabAt(1).setIcon(android.R.drawable.alert_light_frame);
        }


    	checkExistence = this.getResources().getIdentifier("tab1", "drawable", this.getPackageName());
        if ( checkExistence != 0 ) {
            tabLayout.getTabAt(0).setIcon(this.getResources().getIdentifier("tab1", "drawable", this.getPackageName()));
        }
        else {
            // tabLayout.getTabAt(0).setIcon(android.R.drawable.alert_light_frame);
        }


    	checkExistence = this.getResources().getIdentifier("tab3", "drawable", this.getPackageName());
        if ( checkExistence != 0 ) {
            tabLayout.getTabAt(2).setIcon(this.getResources().getIdentifier("tab3", "drawable", this.getPackageName()));
        }
        else {
            // tabLayout.getTabAt(2).setIcon(android.R.drawable.alert_light_frame);
        }

    	tabLayout.getTabAt(3).setIcon(android.R.drawable.btn_star_big_off);

    	tabLayout.getTabAt(4).setIcon(android.R.drawable.btn_star_big_off);



		// Swipe navigation
		RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_Activity_Vex_rW_GRa);
		View viewSwipe = Utils.addSwipeView(relativeLayout, this);
		viewSwipe.setOnTouchListener(new OnSwipeTouchListener(this,viewSwipe));

		View btnLeft = findViewById(R.id.btn_swipe_left);
	    btnLeft.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_heK_r2_NDQ.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_right, R.anim.translate_out_right);
	        finish();
	      }
	    });

	    View btnRight = findViewById(R.id.btn_swipe_right);
	    btnRight.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        Intent intent = new Intent(getApplicationContext(), Activity_zfU_sK_WlU.class);
	        startActivity(intent);
	        overridePendingTransition(R.anim.translate_in_left, R.anim.translate_out_left);
	        finish();
	      }
	    });

		        previousIndex = 1;
    }



    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private Activity_8YX_ce_x5E mPagerActivity;
        private HashMap<Integer, Fragment> tabViewControllers = new HashMap<>();

        public ViewPagerAdapter(FragmentManager fm, Activity_8YX_ce_x5E pagerActivity) {
            super(fm);
            mPagerActivity = pagerActivity;


			Activity_8un_qi_6yE activity_8un_qi_6ye = new Activity_8un_qi_6yE();
			Activity_a6Q_iy_QD2 activity_a6q_iy_qd2 = new Activity_a6Q_iy_QD2();
			Activity_0xt_CY_0hc activity_0xt_cy_0hc = new Activity_0xt_CY_0hc();
			Activity_2qc_If_jg6 activity_2qc_if_jg6 = new Activity_2qc_If_jg6();
			Activity_srg_jB_RBP activity_srg_jb_rbp = new Activity_srg_jB_RBP();
			tabViewControllers.put(1, activity_8un_qi_6ye);
			tabViewControllers.put(0, activity_a6q_iy_qd2);
			tabViewControllers.put(2, activity_0xt_cy_0hc);
			tabViewControllers.put(3, activity_2qc_if_jg6);
			tabViewControllers.put(4, activity_srg_jb_rbp);
       }

        @Override
        public Fragment getItem(int position) {

            return tabViewControllers.get(position);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            //Titles
            switch (position) {

				case 1:
					return "Two";
				case 0:
					return "One";
				case 2:
					return "Three";
				case 3:
					return "featured";
				case 4:
					return "favorites";
                default:
                    return "";
            }
        }

        @Override
        public int getCount() {
            return tabViewControllers.size();
        }
    }
}

